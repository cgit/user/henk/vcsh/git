[include]
  # for [user] section with email, realname, signing key, etc
  path = personal.conf

[branch]
  # always make new branches rebase on their tracking branch
  autosetuprebase = always
[commit]
  # show diff in commit message editor
  verbose = true
  # use a scissor marker, so comments (markdown formatting) works in commit message
  cleanup = scissors
[core]
  # complain about tabs used for indenting
  # commented because I switched to tabs
  # and yaml can’t use tabs
  # so either variant will  complain
  # whitespace = tab-in-indent
[diff]
  # use (i)ndex, (w)orktree, (c)ommit, etc instead of a/ and b/
  mnemonicprefix = true
  # detect renames and copies
  renames = copies
  # use different color for moved code (default == zebra, currently)
  colorMoved = default
  # use vimdiff as difftool
  tool = vimdiff
  # possibly more verbose but also better to understand
  algorithm = histogram
[difftool]
  # do not prompt before invoking difftool
  prompt = false
[fetch]
  # delete tracking references and info about remote branches
  # does not delete any branches
  prune = true
[format]
  # show a lot of info by default
  pretty=compact
[grep]
  # show linenumber (-n)
  lineNumber = true
[init]
  defaultBranch = main
[log]
  # show short commit sha
  abbrevCommit = true
  # show short ref names but only in terminal
  decorate = auto
[merge]
  # use vimdiff as mergetool
  tool = vimdiff
  # this shows the ancestor
  conflictstyle = diff3
[pretty]
  compact = %C(auto)%h%Cgreen% cs%Cblue%<(15,trunc)% an%C(auto)%d%Creset% s
[push]
  # push to branch with same name on remote
  default = current
  # also push reachable annotated tags
  followTags = true
[pull]
  # rebase, don’t merge, local changes
  rebase = true
  # only accept fastforwards by default
  ff = only
[rebase]
  # when in a dirty worktree, automatically create a stash before and apply after rebasing
  autoStash = true
  # automatically squash marked commits (squash! …, fixup! …)
  autoSquash = true
[rerere]
  # don’t use rerere to resolve merge conflicts
  enabled = false
[transfer]
  # check some stuff in the repo when transferring it
  fsckObjects = true

[alias]
  a  = add
  aa = add --all
  ai = add --interactive
  ap = add --patch
  au = add --update

  b      = branch
  br     = branch
  bra    = branch --all
  brav   = branch --all --verbose --verbose

  co  = checkout

  cp  = cherry-pick
  cpa = cherry-pick --abort
  cpc = cherry-pick --continue

  c     = commit
  ca    = commit --all
  caa   = commit --all --amend
  c-a   = commit --amend
  cm    = commit --message
  cam   = commit --all --message
  fix   = commit --all --fixup

  d     = diff --word-diff
  dc    = diff --cached
  dlc   = diff --cached HEAD^
  ds    = diff --staged
  dr    = "!f() { git diff -w "$1"^.."$1"; }; f"
  diffr = "!f() { git diff "$1"^.."$1"; }; f"

  # grep on filename
  f = "!git ls-files | grep -i"

  g    = grep -I
  gr   = grep -I
  gra  = "!sh -c 'git grep "$0" "$@" "$(git for-each-ref --format=\"%(refname)\"" refs/)'"
  grep = grep -I

  log     = log --graph
  l       = log --graph
  la      = log --graph --all
  lap     = log --graph --all --patch
  lp      = log --patch
  ls      = log --stat
  # tag logs
  taglog = "log --tags --simplify-by-decoration --pretty='format:%ci %d'"

  pl  = pull
  pla = pull --all

  ps     = push
  please = push --force-with-lease

  rem  = remote
  remp = remote prune
  remu = remote update

  frbp = "!f() { git fetch; git rebase "$1"; git push --force-with-lease; }; f"
  rb   = rebase
  rba  = rebase --abort
  rbc  = rebase --continue
  rbi  = rebase --interactive
  rbm = "!f() { git switch "$1" && git rebase - && git switch - && git merge -; }; f"

  r   = reset
  r1  = reset HEAD^
  r2  = reset HEAD^^
  rh  = reset --hard
  rh1 = reset --hard HEAD^
  rh2 = reset --hard HEAD^^

  rs = restore

  sl = stash list
  sa = stash apply
  sp = stash pop
  ss = stash save

  st = status --short

  sw = switch
  swc = switch --create

  # add subtree
  sba = "!f() { git subtree add --prefix $2 $1 master --squash; }; f"
  # update/pull subtree
  sbu = "!f() { git subtree pull --prefix $2 $1 master --squash; }; f"

  # initial empty commit
  empty = "!git commit -am\"[empty] Initial commit\" --allow-empty"

  wl = worktree list

  serve = daemon --reuseaddr --verbose --base-path=. --export-all ./.git
